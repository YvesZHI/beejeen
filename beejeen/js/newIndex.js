var html = '';
var data_response = '';
var hidTimer = true;
var mudiFlag = true;
var xingFlag = false;
var tempurl;
$(function() {

  //获取屏幕高度，使iframe窗口的高度自适应屏幕高度
  var wHt = $(window).height();
  var iframeNode = document.getElementById('iframe-top');
  var iframeFaceNode = document.getElementById('iframe-face');
  iframeNode.style.height = wHt * 80 / 100 + 'px';
  iframeFaceNode.style.height = wHt * 80 / 100 + 'px';

  if(navigator.userAgent.indexOf('Chrome') == -1){
    console.log(navigator.userAgent)
    var spWidth = $('.swiper-container1').css('width');
    console.log(spWidth);
    var spHeight = parseFloat(spWidth)*32/100;
    console.log(spHeight);
    $('.swiper-container1').css('height',spHeight);
  }

  // 给顶部显示国家菜单添加事件
  $('.destination').on('click', function() {
    $('#navigation').show();
    $.get('http://www.beejeen.com/look?lang=cn', function(data) {
      console.log(data);
      var data2 = JSON.parse(data);
      console.log(data2)
      var navObj = $('.nav-bar');
      var navBarArry = [];
      var navItemArry = [];
      var ItemInnerArry = [];


      console.log(data2.navigation.length);
      for (var i = 0; i < data2.navigation.length; i++) {
        console.log(data2.navigation[i].name);
        navBarArry.push('<li>' + data2.navigation[i].name + '</li>');
        console.log('bbbbbbbbnnnnnnnnn')
        let abc = [];
        for (var m = 0; m < data2.navigation[i].item.length; m++) {
          abc.push('<a>' + '<span>' + data2.navigation[i].item[m] + '</span>' + '</a>');
        }
        navItemArry.push('<li>' + abc.join('') + '</li>')
      }
      console.log(navBarArry.join(''))
      console.log(navItemArry.join(''))
      $('.nav-bar').html(navBarArry.join(''));
      $('.nav-item').html(navItemArry.join(''));
      var anode = $('.nav-item a');
      $.each(anode, function(i) {
        $(this).attr('href', 'http://www.beejeen.com/html/country-cn.html?' + $(this).find('span').text());
      })

      $('#navigation>ul.nav-item>li').eq(0).addClass('current').siblings().removeClass('current');
      $('#navigation>ul.nav-bar>li').eq(0).addClass('current').siblings().removeClass('current');


      add_event();
    });


    zfx.showFullScreenMask();


  });

  // 切换目的地和行程按钮
  $('.ck-mudi').on('click', function() {
    $(this).find('i').addClass('ck-icon-color');
    $('.icon-hangcheng').removeClass('ck-icon-color');
    mudiFlag = true;
    xingFlag = false;
  });
  $('.ck-xing').on('click', function() {
    $(this).find('i').addClass('ck-icon-color');
    $('.icon-mudedichanpin').removeClass('ck-icon-color');
    mudiFlag = false;
    xingFlag = true;
  });


  // 切换中英文
  $('#language').on('click', function() {
    if ($(this).text() == ('EN')) {
      $(this).text('中文');
      displayLanguage('EN');
    } else {
      $(this).text('EN');
      displayLanguage('中');
    }
  });

  function displayLanguage(language) {
    if (language == 'EN') {
      localStorage.setItem('beejeen-language', 'en');
      $('#main-sh').attr('placeholder', 'Where would you like to go');
      $('#main-lookfor').text('Search');
      $('.ck-mudi span').text('destination');
      $('.ck-xing span').text('trip');

    } else {
      localStorage.setItem('beejeen-language', 'cn');
      $('#main-sh').attr('placeholder', '搜一搜你想去哪happy');
      $('#main-lookfor').text('搜索');
      $('.ck-mudi span').text('目的地');
      $('ck-xing span').text('行程');
    }
  }




  // 最顶部搜索图标鼠标动画
  $('#hidden-search').on('focus', function() {
    $('.search-top').addClass('addC');
  });
  $('#hidden-search').on('blur', function() {
    $('.search-top').removeClass('addC');
  })
  // $('.search-top').on('click', function() {
  //   console.log('dddddd')
  //   $('.search-top').addClass('addC');
  // })

  $('#tophs').on('click', function() {
    if ($('#hidden-search').val() == '') {
      return false;
    }
    var ifhave = localStorage.getItem('historyObj');
    if (ifhave) {
      var skeywo = $('#hidden-search').val();
      var ifhaveObj = JSON.parse(ifhave);
      console.log(ifhaveObj);
      var flagN = true;
      for(var i = 0;i < ifhaveObj.history.length; i++){
        if(ifhaveObj.history[i]==skeywo){
          flagN = false;
        }
      }
      if(flagN){
        ifhaveObj.history.push(skeywo);
        console.log(ifhaveObj);
        var ifhaveStr = JSON.stringify(ifhaveObj);

        localStorage.setItem('historyObj', ifhaveStr);
      }


    } else {
      var skeywo = $('#hidden-search').val();
      var obj = {
        history: []
      }
      obj.history.push(skeywo);
      var str = JSON.stringify(obj);
      localStorage.setItem('historyObj', str);
    }

    $.ajax({
      url: 'http://www.beejeen.com/search?key=' + $('#hidden-search').val() + '&types=0',
      type: 'get',
      cache: true,
      success: function(data) {
        console.log(data);
        var data2 = JSON.parse(data);
        console.log(data2);
        location.href = data2.link;
      },
      error: function(jqXHR) {
        console.log(jqXHR);
      }
    });

    // $.get('http://www.beejeen.com/search?key=' + $('#hidden-search').val() + '&types=0',function(data) {
    //
    //   console.log(data);
    //      var data2 = JSON.parse(data);
    //      console.log(data2);
    //      location.href = data2.link;
    // })

  })


  // 对搜索下拉框中出现的历史记录关键词添加动态节点事件，为动态节点添加事件形式如下
  // 利用trigger触发再次搜索
  $('.hidden-mes-text').on('mousedown', 'span', function() {
    console.log('llllive')
    $('#main-sh').val($(this).text());
    $('#main-sh').trigger('focus');
  })

  // 搜索框聚焦事件,搜索历史记录不再按照接口请求
  $('#main-sh').on('focus', function() {
    hidTimer = true;
    if ($('#main-sh').val() == '') {
      $('.loading').hide();
      var dataHyStr = localStorage.getItem('historyObj');
      if (dataHyStr) {
        var dataHyObj = JSON.parse(dataHyStr);
        var worlist = [];
        for (var i = 0; i < dataHyObj.history.length; i++) {
          worlist.push('<span>' + dataHyObj.history[i] + '</span>');
        }
        $('.hidden-mesbody').hide();
        $('.hidden-mes-text').html(worlist.join(''));
        // $('.hidden')
        $('.hidden-mes-top').show();
        $('.hidden-mes-text').show();
        $('.hidden-mes').show();
      } else {

        // $('.hidden-mesbody').hide();
        // $('.hidden-mes-top').show();
        $('.hidden-mes').hide();
      }
      // else{
      //   $.ajax({
      //     url: 'http://www.beejeen.com/history?sess_id=1111&limit=9',
      //     method: 'get',
      //     cache: true,
      //     success: function(data) {
      //       console.log(data);
      //       if (data) {
      //         // var data_array = eval("("+data+")");
      //         var dataStr = JSON.stringify(data);
      //         localStorage.setItem('historyObj',dataStr);
      //         var data_array = data;
      //         var worlist = [];
      //         for (var i = 0; i < data_array.hisory.length; i++) {
      //           worlist.push('<span>' + data_array.hisory[i].keyword + '</span>');
      //         }
      //         $('.hidden-mesbody').hide();
      //         $('.hidden-mes-text').html(worlist.join(''));
      //         // $('.hidden')
      //         $('.hidden-mes-top').show();
      //         $('.hidden-mes-text').show();
      //         $('.hidden-mes').show();
      //       }
      //     },
      //     error: function(jqXHR) {
      //       console.log(jqXHR);
      //     }
      //   });
      //
      // }

    } else {
      // $('.loading').show();

      if (mudiFlag == true) {
        $.ajax({
          url: 'http://www.beejeen.com/search?key=' + $('#main-sh').val() + '&types=1',
          method: 'get',
          cache: true,
          success: function(data) {
            console.log(data);
            if (!(data.country==null&&data.city==null&&data.sight.length==0)) {
              // var data_array = eval("("+data+")");
              // var data_array = data;
              var worlist = [];
              if (data.country) {
                worlist.push('<div class="hid-new">' + '<a ' + 'target="_blank" href=' + data.country.link + '>' + '<span>' + data.country.name_cn + '</span></a>' + '<span>国家</span>' + '</div>');

              }
              if (data.city) {
                worlist.push('<div class="hid-new">' + '<a ' + 'target="_blank" href=' + data.city.link + '>' + '<span>' + data.city.name_cn + '</span></a>' + '<span>城市</span>' + '</div>');

              }
              if (data.sight) {
                for (var i = 0; i < data.sight.length; i++) {
                  worlist.push('<div class="hid-new">' + '<a ' + 'target="_blank" href=' + data.sight[i].link + '>' + '<span>' + data.sight[i].name_cn + '</span></a>' + '<span>' + data.sight[i].city + ' | 景点</span>' + '</div>');
                }

              }
              $('.loading').hide();

              $('.hidden-mes-top').hide();
              $('.hidden-mes-text').hide();
              $('.hidden-mesbody').html(worlist.join(''));
              $('.hidden-mes').show();
              $('.hidden-mesbody').show();

            }else{
              $('.loading').show();
            }
          },
          error: function(jqXHR) {
            console.log(jqXHR);
          }
        });

      }
      if (xingFlag == true) {
        $.ajax({
          url: 'http://www.beejeen.com/travel?key=' + $('#main-sh').val() + '&types=1',
          method: 'get',
          cache: true,
          success: function(data) {
            console.log(data);
            if (!(data.country==null&&data.city==null&&data.sight.length==0)) {
              // var data_array = eval("("+data+")");
              // var data_array = data;
              var worlist = [];

              if (data.city) {
                worlist.push('<div class="hid-new">' + '<a ' + 'href=' + data.city.link + '>' + '<span>' + data.city.name_cn + '</span></a>' + '</div>');

              }
              if (data.travel) {
                for (var i = 0; i < data.travel.length; i++) {
                  worlist.push('<div class="hid-new">' + '<a ' + 'target="_blank" href=' + data.travel[i].link + '>' + '<span>' + data.travel[i].name_cn + '</span></a>' + '</div>');
                }

              }
              $('.loading').hide();

              $('.hidden-mes-top').hide();
              $('.hidden-mes-text').hide();
              $('.hidden-mesbody').html(worlist.join(''));
              $('.hidden-mes').show();
              $('.hidden-mesbody').show();


            }else{
              $('.loading').show();

            }
          },
          error: function(jqXHR) {
            console.log(jqXHR);
          }
        });
      }
    }


  })
  // 清除历史记录
  $('.hidden-mes-top i').on('click', function() {
    $('.hidden-mes-text').html('');
    localStorage.removeItem('historyObj');
  })

  // 搜索框失去焦点事件利用全局flag
  $('#main-sh').on('blur', function() {
    setTimeout(function() {
      if (hidTimer) {

        $('.hidden-mes').hide();
      }
    }, 500);
    console.log(hidTimer)
  })
  $('.hidden-mes').on('mousedown', function() {
    console.log(hidTimer)
    hidTimer = false;
    console.log(hidTimer)
  })


  // 利用body点击实现隐藏，遇到iframe受阻,解决方案是在iframe上面设置蒙层
  $("body").on("click", function(evt) {
    if ($(evt.target).parents(".center-search").length == 0 && $(evt.target).parents(".hidden-mes").length == 0 && $(evt.target).prop('className') != 'hidden-mes') {
      console.log($(evt.target).prop('className'));
      $('.hidden-mes').hide();
      // if($(evt.target).prop('className')!='search-top'){
      //   $('.search-top').removeClass('addC');
      // }
    }

  });

  // 定义iframe相关的触发监听
  // var IframeOnClick = {
  //     resolution: 200,
  //     iframes: [],
  //     interval: null,
  //     Iframe: function() {
  //         this.element = arguments[0];
  //         this.cb = arguments[1];
  //         this.hasTracked = false;
  //     },
  //     track: function(element, cb) {
  //         this.iframes.push(new this.Iframe(element, cb));
  //         if (!this.interval) {
  //             var _this = this;
  //             this.interval = setInterval(function() { _this.checkClick(); }, this.resolution);
  //         }
  //     },
  //     checkClick: function() {
  //         if (document.activeElement) {
  //             var activeElement = document.activeElement;
  //             for (var i in this.iframes) {
  //                 if (activeElement === this.iframes[i].element) { // user is in this Iframe
  //                     if (this.iframes[i].hasTracked == false) {
  //                         this.iframes[i].cb.apply(window, []);
  //                         this.iframes[i].hasTracked = true;
  //                     }
  //                 } else {
  //                     this.iframes[i].hasTracked = false;
  //                 }
  //             }
  //         }
  //     }
  // };
  //
  // IframeOnClick.track(document.getElementById("iframe-top"), function() {
  //   $('#iframe-top').on('click',function(){
  //     console.log('aaaaaaaa');
  //   })
  //  });


  // $('#iframe-top').on('load',function() {
  //   $(window.frames[“iframe-top”].document).find(“body”).click(function() {
  //     $(window.parent.document).find(“.hidden-mes”).hide();
  //   })
  // });

  // 搜索框中不断触发查询的事件
  $('#main-sh').on('keyup', function(event) {
    if ($('#main-sh').val() == '') {
      $('#main-sh').trigger('focus');
      return false;
    }
    if(event.keyCode==13){
      $('#main-lookfor').trigger('click');
      return false;
    }
    // $('.hidden-mes-top').hide();
    // $('.hidden-mes-text').hide();
    // $('.hidden-mesbody').show();
    console.log('key up ing');
    $('.hidden-mes-top').hide();
    $('.hidden-mes-text').hide();
    $('.hidden-mesbody').hide();

    $('.loading').show();
    if (mudiFlag == true) {
      $.ajax({
        url: 'http://www.beejeen.com/search?key=' + $('#main-sh').val() + '&types=1',
        method: 'get',
        cache: true,
        success: function(data) {
          console.log(data);
          if (!(data.country==null&&data.city==null&&data.sight.length==0)) {
            // var data_array = eval("("+data+")");
            // var data_array = data;
            var worlist = [];


            if (data.country) {
              worlist.push('<div class="hid-new">' + '<a ' + 'target="_blank" href=' + data.country.link + '>' + '<span>' + data.country.name_cn + '</span></a>' + '<span>国家</span>' + '</div>');

            }
            if (data.city) {
              worlist.push('<div class="hid-new">' + '<a ' + 'target="_blank" href=' + data.city.link + '>' + '<span>' + data.city.name_cn + '</span></a>' + '<span>城市</span>' + '</div>');

            }
            if (data.sight) {
              for (var i = 0; i < data.sight.length; i++) {
                worlist.push('<div class="hid-new">' + '<a ' + 'target="_blank" href=' + data.sight[i].link + '>' + '<span>' + data.sight[i].name_cn + '</span></a>' + '<span>' + data.sight[i].city + ' | 景点</span>' + '</div>');
              }

            }
            $('.loading').hide();
            $('.hidden-mes-top').hide();
            $('.hidden-mes-text').hide();
            $('.hidden-mesbody').html(worlist.join(''));
            $('.hidden-mesbody').show();
            $('.hidden-mes').show();
          }else{
            $('.loading').show();
          }
        },
        error: function(jqXHR) {
          console.log(jqXHR);
        }
      });

    }
    if (xingFlag == true) {
      $.ajax({
        url: 'http://www.beejeen.com/travel?key=' + $('#main-sh').val() + '&types=1',
        method: 'get',
        cache: true,
        success: function(data) {
          console.log(data);
          if (!(data.country==null&&data.city==null&&data.sight.length==0)) {
            // var data_array = eval("("+data+")");
            // var data_array = data;
            var worlist = [];

            if (data.city) {
              worlist.push('<div class="hid-new">' + '<a ' + 'href=' + data.city.link + '>' + '<span>' + data.city.name_cn + '</span></a>' + '</div>');

            }
            if (data.travel) {
              for (var i = 0; i < data.travel.length; i++) {
                worlist.push('<div class="hid-new">' + '<a ' + 'target="_blank" href=' + data.travel[i].link + '>' + '<span>' + data.travel[i].name_cn + '</span></a>' + '</div>');
              }

            }
            $('.loading').hide();
            $('.hidden-mes-top').hide();
            $('.hidden-mes-text').hide();
            $('.hidden-mesbody').html(worlist.join(''));
            $('.hidden-mesbody').show();
            $('.hidden-mes').show();


          }else{
            $('.loading').show();
          }
        },
        error: function(jqXHR) {
          console.log(jqXHR);
        }
      });
    }
  });

  // 主搜索按钮的事件
  $('#main-lookfor').on('click', function() {


    if ($('#main-sh').val() == '') {
      return false;
    }

    tempurl = window.open();

    var ifhave = localStorage.getItem('historyObj');
    if (ifhave) {
      var skeywo = $('#main-sh').val();
      var ifhaveObj = JSON.parse(ifhave);
      console.log(ifhaveObj);

      var flagN = true;
      for(var i = 0;i < ifhaveObj.history.length; i++){
        if(ifhaveObj.history[i]==skeywo){
          flagN = false;
        }
      }
      if(flagN){
        ifhaveObj.history.push(skeywo);
        console.log(ifhaveObj);
        var ifhaveStr = JSON.stringify(ifhaveObj);

        localStorage.setItem('historyObj', ifhaveStr);
      }

    } else {
      var skeywo = $('#main-sh').val();
      var obj = {
        history: []
      }
      obj.history.push(skeywo);
      var str = JSON.stringify(obj);
      localStorage.setItem('historyObj', str);
    }


    // $('.hidden-mes-top').hide();
    // $('.hidden-mes-text').hide();
    // $('.hidden-mesbody').show();
    console.log('look for ing');
    if (mudiFlag == true) {
      $.ajax({
        url: 'http://www.beejeen.com/search?key=' + $('#main-sh').val() + '&types=0',
        method: 'get',
        cache: true,
        success: function(data) {
          console.log(data);
          var data2 = JSON.parse(data);
          console.log(data2);
          tempurl.location = data2.link;


          // var abf = $("<a href="+ 'https://www.baidu.com' +" target='_blank'>Apple</a>").get(0);
          // var eaa = document.createEvent('MouseEvents');
          // eaa.initEvent( 'click', true, true );
          // abf.dispatchEvent(eaa);
          // var neu = "<a href="+ data2.link +" target='_blank'>Apple</a>";


          // $('#main-lookfor').parents('a').attr('onclick' , 'window.open(' + data2.link + ')' );
          //   if (data) {
          // // var data_array = eval("("+data+")");
          // // var data_array = data;
          //   var worlist = [];
          //   if (data.country) {
          //     worlist.push('<div class="hid-new">' + '<a ' + 'target="_blank" href=' + data.country.link + '>' + '<span>' + data.country.name_cn + '</span></a>' + '</div>');
          //
          //   }
          //   if (data.city) {
          //     worlist.push('<div class="hid-new">' + '<a ' + 'target="_blank" href=' + data.city.link + '>' + '<span>' + data.city.name_cn + '</span></a>' + '</div>');
          //
          //   }
          //   if (data.sight) {
          //     for (var i = 0; i < data.sight.length; i++) {
          //     worlist.push('<div class="hid-new">' + '<a ' + 'target="_blank" href=' + data.sight[i].link + '>' + '<span>' + data.sight[i].name_cn + '</span></a>' + '</div>');
          //     }
          //
          //   }
          //   $('.hidden-mes').html(worlist.join(''));
          // }
        },
        error: function(jqXHR) {
          console.log(jqXHR);
        }
      });

    }
    if (xingFlag == true) {
      $.ajax({
        url: 'http://www.beejeen.com/travel?key=' + $('#main-sh').val() + '&types=0',
        method: 'get',
        cache: true,
        success: function(data) {
          console.log(data);
          var data2 = JSON.parse(data);
          console.log(data2);
          // window.open(data2.travel[0].link);
          tempurl.location = data2.travel[0].link;
        


          //   if (data) {
          // // var data_array = eval("("+data+")");
          // // var data_array = data;
          //   var worlist = [];
          //
          //   if (data.city) {
          //     worlist.push('<div class="hid-new">' + '<a ' + 'href=' + data.city.link + '>' + '<span>' + data.city.name_cn + '</span></a>' + '</div>');
          //
          //   }
          //   if (data.travel) {
          //     for (var i = 0; i < data.travel.length; i++) {
          //     worlist.push('<div class="hid-new">' + '<a ' + 'target="_blank" href=' + data.travel[i].link + '>' + '<span>' + data.travel[i].name_cn + '</span></a>' + '</div>');
          //     }
          //
          //   }
          //   $('.hidden-mes').html(worlist.join(''));
          // }
        },
        error: function(jqXHR) {
          console.log(jqXHR);
        }
      });
    }

  });



  $.get('http://www.beejeen.com/look?lang=cn', function(data) {
    console.log(data);
    var data2 = JSON.parse(data);
    var leftObj = $('.left-img');
    var h3Obj = $('.right-text h3');
    var placeObj = $('.tr-place');
    var contentObj = $('.tr-content');
    var costObj = $('.tr-cost span');
    var aObj = $('.tr-detail');
    $.each(leftObj, function(i) {
      $(this).css({
        'width': '60%',
        'height': '100%',
        'float': 'left',
        'background-size': 'cover',
        'background-repeat': 'no-repeat'
      });
      $(this).css('background-image', 'url(' + data2.itinerary[i].img + ')');
      $(this).siblings('.right-text').find('h3').html(data2.itinerary[i].title + '');
      $(this).siblings('.right-text').find('.tr-place').html(data2.itinerary[i].country + '');
      $(this).siblings('.right-text').find('.tr-content').html(data2.itinerary[i].content + '');
      $(this).siblings('.right-text').find('.tr-cost span').html(data2.itinerary[i].cost + '');
      $(this).siblings('.right-text').find('.tr-detail').attr('href', 'html/' + data2.itinerary[i]['a-href'] + '');
      $(this).siblings('.right-text').find('.tr-detail').attr('target', '_blank');
    })

  })


  $.get('http://www.beejeen.com/hotcountry?limit=10', function(data) {
    console.log(data);
    var imgObj = $('.hoimg');
    var titleObj = $('.hotitle');
    var wordObj = $('.howord');

    $.each(imgObj, function(i) {
      $(this).css({
        'width': '100%',
        'height': '0',
        'padding-bottom': '150%',
        'margin-bottom': '5px',
        'background-position': 'center center',
        'background-size': 'cover',
        'background-repeat': 'no-repeat'
      })
      $(this).css('background-image', 'url(' + data.hotcountry[i].thumb_link + ')');
      $(this).parents('a').attr('href', data.hotcountry[i].link + '');
      $(this).parents('a').attr('target', '_blank');
    })
    $.each(titleObj, function(i) {
      $(this).css({
        'padding-left': '3px',
        'font-size': '20px',
        'color': '#333333'
      })
      $(this).html(data.hotcountry[i].name_cn + '');
    })
    $.each(wordObj, function(i) {
      $(this).css({
        'font-size': '14px',
        'color': '#bababa',
        'overflow': 'hidden',
        'text-overflow': 'ellipsis',
        'white-space': 'nowrap',
        'margin-bottom': '5px'
      })

      $(this).html(data.hotcountry[i].text_cn + '');
    })

    // $('.swipe').attr('src',data.hotcountry[1].thumb_link);
  })

  $.get('http://www.beejeen.com/taglist?tag_id=1', function(data) {
    var timgObj = $('.theme-img');
    var titObj = $('.theme-word-title');
    var texObj = $('.theme-word-text');

    $.each(timgObj, function(i) {
      $(this).css({
        'width': '100%',
        'height': '80%'
      })
      $(this).css('background', 'url(' + data.taglist[i].thumb_link + ')  center center no-repeat');
      $(this).parents('a').attr('href', data.taglist[i].link + '');
      $(this).parents('a').attr('target', '_blank');
    })
    $.each(titObj, function(i) {
      $(this).html(data.taglist[i].name_cn + '');
    })
    $.each(texObj, function(i) {
      $(this).html(data.taglist[i].intro_cn + '');
    })


  });

  //主题玩法的标签切换事件
  $('#qinzi').on('click', function() {
    $(this).addClass('iclick').siblings('i').removeClass('iclick');
    $.get('http://www.beejeen.com/taglist?tag_id=2', function(data) {
      var timgObj = $('.theme-img');
      var titObj = $('.theme-word-title');
      var texObj = $('.theme-word-text');

      $.each(timgObj, function(i) {
        $(this).css({
          'width': '100%',
          'height': '80%'
        })
        $(this).css('background', 'url(' + data.taglist[i].thumb_link + ')  center center no-repeat');
        $(this).parents('a').attr('href', data.taglist[i].link + '');
      })
      $.each(titObj, function(i) {
        $(this).html(data.taglist[i].name_cn + '');
      })
      $.each(texObj, function(i) {
        $(this).html(data.taglist[i].intro_cn + '');
      })
    });
  });

  $('#zongjiao').on('click', function() {
    $(this).addClass('iclick').siblings('i').removeClass('iclick');
    $.get('http://www.beejeen.com/taglist?tag_id=5', function(data) {
      var timgObj = $('.theme-img');
      var titObj = $('.theme-word-title');
      var texObj = $('.theme-word-text');

      $.each(timgObj, function(i) {
        $(this).css({
          'width': '100%',
          'height': '80%'
        })
        $(this).css('background', 'url(' + data.taglist[i].thumb_link + ')  center center no-repeat');
        $(this).parents('a').attr('href', data.taglist[i].link + '');
      })
      $.each(titObj, function(i) {
        $(this).html(data.taglist[i].name_cn + '');
      })
      $.each(texObj, function(i) {
        $(this).html(data.taglist[i].intro_cn + '');
      })
    });
  });

  $('#qinglv').on('click', function() {
    $(this).addClass('iclick').siblings('i').removeClass('iclick');
    $.get('http://www.beejeen.com/taglist?tag_id=3', function(data) {
      var timgObj = $('.theme-img');
      var titObj = $('.theme-word-title');
      var texObj = $('.theme-word-text');

      $.each(timgObj, function(i) {
        $(this).css({
          'width': '100%',
          'height': '80%'
        })
        $(this).css('background', 'url(' + data.taglist[i].thumb_link + ')  center center no-repeat');
        $(this).parents('a').attr('href', data.taglist[i].link + '');
      })
      $.each(titObj, function(i) {
        $(this).html(data.taglist[i].name_cn + '');
      })
      $.each(texObj, function(i) {
        $(this).html(data.taglist[i].intro_cn + '');
      })
    });
  });

  $('#guji').on('click', function() {
    $(this).addClass('iclick').siblings('i').removeClass('iclick');
    $.get('http://www.beejeen.com/taglist?tag_id=4', function(data) {
      var timgObj = $('.theme-img');
      var titObj = $('.theme-word-title');
      var texObj = $('.theme-word-text');

      $.each(timgObj, function(i) {
        $(this).css({
          'width': '100%',
          'height': '80%'
        })
        $(this).css('background', 'url(' + data.taglist[i].thumb_link + ')  center center no-repeat');
        $(this).parents('a').attr('href', data.taglist[i].link + '');
      })
      $.each(titObj, function(i) {
        $(this).html(data.taglist[i].name_cn + '');
      })
      $.each(texObj, function(i) {
        $(this).html(data.taglist[i].intro_cn + '');
      })
    });
  });

  $('#yelin').on('click', function() {
    $(this).addClass('iclick').siblings('i').removeClass('iclick');
    $.get('http://www.beejeen.com/taglist?tag_id=1', function(data) {
      var timgObj = $('.theme-img');
      var titObj = $('.theme-word-title');
      var texObj = $('.theme-word-text');

      $.each(timgObj, function(i) {
        $(this).css({
          'width': '100%',
          'height': '80%'
        })
        $(this).css('background', 'url(' + data.taglist[i].thumb_link + ')  center center no-repeat');
        $(this).parents('a').attr('href', data.taglist[i].link + '');
      })
      $.each(titObj, function(i) {
        $(this).html(data.taglist[i].name_cn + '');
      })
      $.each(texObj, function(i) {
        $(this).html(data.taglist[i].intro_cn + '');
      })
    });
  });

$('#dingzhi-info-submit').on('click',function() {
  var name = $('#dingzhi-name').val()
  var des = $('#dingzhi-des').val()
  var day = $('#dingzhi-day').val()
  var phone = $('#dingzhi-phone').val()
  var mes= $('#dingzhi-beizhu').val()

  if(name == ''&& des==''&& day==''&& phone==''&& mes==''){
    console.log('111111111');
    return false;
  }
  console.log('222222222');
  var data = new FormData();
  data.append('name',name);
  data.append('destination',des);
  data.append('day',day);
  data.append('phone',phone);
  data.append('message',mes);

  $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "http://www.beejeen.com/message",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 5000,
            success: function(data){
                console.log(data);
                var data2 = JSON.parse(data);
                $('.dingzhi-info-first').hide();
                $('.dingzhi-info-second').hide();
                $('.dingzhi-info-third').hide();
                $('#dingzhi-info-submit').hide();
                $('.dingzhi-suc-top').text(data2.msg);
                $('.dingzhi-suc').show();

                if(data.status == '200'){
                }


            },
            error: function(e){
                console.log(e);
            }
    });

});

$('.dingzhi-suc-down').on('click',function() {
  $('#dingzhi-name').val('');
  $('#dingzhi-des').val('');
  $('#dingzhi-day').val('');
  $('#dingzhi-phone').val('');
  $('#dingzhi-beizhu').val('');
  $('.dingzhi-info-first').show();
  $('.dingzhi-info-second').show();
  $('.dingzhi-info-third').show();
  $('#dingzhi-info-submit').show();
  $('.dingzhi-suc').hide();
})


  $('body').on('click', '#input-search', function() {
    search(encodeURIComponent($('#input-text').val()));
  });

  $('body').on('keypress', '#input-text', function(e) {
    if (e.keyCode == 13) {
      search(encodeURIComponent($('#input-text').val()));
    }
  });

});

// function request(request) {
//   $.ajax({
//     url: 'http://www.beejeen.com/country?key=' + request,
//     type: 'get',
//     async: true,
//     timeout: 5000,
//     dataType: 'json',
//     beforeSend: function(xhr) {
//       console.log(xhr)
//       console.log('发送前')
//     },
//     success: function(data, textStatus, jqXHR) {
//       console.log('成功')
//       console.log(data)
//       console.log(textStatus)
//       console.log(jqXHR)
//       data_response = data;
//       if (!!data.link) {
//         location.href = data.link.replace('.html', '-cn.html');
//       } else {
//
//         // data = JSON.parse(data);
//         document.title = data.country.name;
//
//         apppend_data(data, 'body');
//         add_event();
//       }
//     },
//     error: function(xhr, textStatus) {
//       console.log('错误')
//       console.log(xhr)
//       console.log(textStatus)
//     },
//     complete: function() {
//       console.log('结束')
//     }
//   });
// }

// function apppend_data(data, position) {
//   html = template('body_tpl', data);
//   $(position).html(html);
// }

function add_event() {

  $('#navigation>ul.nav-bar>li').on('click', function() {
    $(this).addClass('current').siblings().removeClass('current');
    $('#navigation>ul.nav-item>li').eq($(this).index()).addClass('current')
      .siblings().removeClass('current');
  });

  $('.nav-close').on('click', function() {
    $('#navigation').hide();
    zfx.hideFullScreenMask();
    //  	$('html').removeClass('fixed');
  });

  $('#navigation span').on('click', function() {
    search(encodeURIComponent($(this).text()));
  });

  $('#navigation>input').on('click', function() {
    search(encodeURIComponent($(this).val()));
  });

  $('#navigation>input').on('keypress', function(e) {
    if (e.keyCode == 13) {
      search(encodeURIComponent($(this).val()));
    }
  });

}

//全屏、蒙版 封装模块
(function() {
  var zfx = {

    //FullScreenMask
    hasFullScreenMask: false,
    maskId: '#full-screen-mask',

    showFullScreenMask: function() {
      var str = '<div id="full-screen-mask" style="position:fixed;top:0;left:0;width: 100%;height: 100%;background: rgba(0,0,0,0.4);z-index: 1000;"></div>'
      $('body').append(str);
      this.hasFullScreenMask = true;
    },

    hideFullScreenMask: function() {
      $('#full-screen-mask').remove();
      this.hasFullScreenMask = false;
    },

    toggleFullScreenMask: function() {
      this.hasFullScreenMask ? this.hideFullScreenMask() : this.showFullScreenMask();

    }
  };

  //export api
  window.zfx = zfx;
})();
