#!/usr/local/bin/python3.5
#-*- coding: UTF-8 -*-

import sys
import tornado.web

import urllib.parse
import tornado.escape

sys.path.append('/var/server/beejeen/model')

from Country import Country


def getLook(lang):
    try:
        result = Look().getJson(lang)
        print(result)
    except Exception as e:
        print('EXCEPTION FROM getLook:')
        print(e)
    return result


class HotCountryHandler(tornado.web.RequestHandler):
    def get(self, path):

        limit = self.get_argument('limit',10)
        try:
            print('HotCountryHandler')

            hot_result = Country.fetchAll( 'ad_cost > 0 ', 'id,name_cn,name_en,text_cn,text_en,thumb_link',limit)

            if hot_result is not  None or len(hot_result)  :
                for hot in hot_result:
                    hot['text_cn'] = hot['text_cn'][0:15]
                    hot['text_en'] = hot['text_en'][0:25]                     
                    hot['link']    ="http://" + self.request.host + "/html/country-cn.html?"+hot['name_cn']


            
            list = {'status': 200, 'hotcountry': hot_result}
            self.write(list)

        except Exception as e:
            print('EXCEPTION FROM HotCountryHandler get:')
            print(e.message)
