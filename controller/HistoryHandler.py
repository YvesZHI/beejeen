#!/usr/local/bin/python3.5
#-*- coding: UTF-8 -*-

import sys
import tornado.web

import urllib.parse
import tornado.escape

sys.path.append('/var/server/beejeen/model')

from History import History



class HistoryHandler(tornado.web.RequestHandler):
    def get(self, path):
        try:

            limit = self.get_argument('limit', 10)
            sess_id = self.get_argument('sess_id', '')

            if sess_id is None:
                self.write('{"status":100,"msg":"请正确操作!"}')

            history_result = History.fetchAll('sess_id=\'' + sess_id + '\'', 'id,keyword,create_time', limit)
            sort = sorted(history_result, key=lambda e: e.get('create_time'), reverse=True)
            list = {'hisory':sort}
            self.write(list)

        except Exception as e:
            print('EXCEPTION FROM HistoryHandler get:')
            print(e)
            self.write('{"status":101,"msg":"服务器繁忙中~!"}')
