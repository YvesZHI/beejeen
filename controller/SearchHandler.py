#!/usr/local/bin/python3.5
#-*- coding: UTF-8 -*-

import re
import sys
import tornado.web
import base64

import urllib.parse
import tornado.escape
import time

sys.path.append('/var/server/beejeen/model')

from Country   import Country
from City      import City
from Sight     import Sight
from History   import History



patternDefense = re.compile(r'.*(create |select |update |alter |delete |drop |show |having | or |<.*>|/).*', re.IGNORECASE)
# 首页搜索内容点击后跳转
class SearchHandler(tornado.web.RequestHandler):
    def get(self, path):
        try:
            print('SearchHandler')
            print(self.request.uri)
            print(self.get_argument('key'))

            types    =  self.get_argument('types','0')
            val      =  tornado.escape.xhtml_escape(self.get_argument('key',''))
            sess_id  = self.get_argument('sess_id', '')

            if  not types : 
                self.write('{"status":100,"msg":"请正确操作!"}')
                return

            if   val is None:
                self.write('{"status":101,"msg":"请正确操作!"}')
                return
            if   sess_id is None:
                self.write('{"status":101,"msg":"请正确操作!"}')
                return

            #记录关键词
            create_time = int(time.time())
  
            history_result = History.addOne(sess_id,val,create_time)

            #val  = tornado.escape.url_unescape(key)
            msg  = urllib.parse.quote(val)
  
            '''
              0 默认搜索
              1 下拉框搜索
            '''
            if types == '0' :
                print("搜索")
                if Country.isCountry(val):
                    
                    self.write('{"link":"http://' + self.request.host + '/html/country-cn.html?' + msg + '"}')

                elif City.isCity(val):
                   
                    self.write('{"link":"http://' + self.request.host + '/html/city-cn.html?' + msg + '"}')

                elif Sight.fetchOne(' name_cn like \''+val+'%\' ','id,name_cn'):
                    
                    self.write('{"link":"http://'+ self.request.host + '/html/tour.html?Sight&key=' + msg + '"}')
                    
                else :
         
                    self.write('{"link":"http://' + self.request.host + '/html/404.html"}')
            else :
                
                country_result =Country.fetchOne('name_cn=\''+val+'\'','id,name_cn')

                if   country_result is not None :
                    
                    country_result['link'] = "http://" + self.request.host + "/html/country-cn.html?" + msg +""


                city_result = City.fetchOne('name_cn=\'' + val + '\'', 'id,name_cn')
                if  city_result is not None :
                    city_result['link'] = "http://" + self.request.host + "/html/city-cn.html?" + msg + ""

                sight_result   = Sight.getCountryAndCityBySid(' s.name_cn like \''+val+'%\' ','s.id,s.name_cn,c.name_cn as counry,t.name_cn as city')

                if sight_result is not None:
                    for sr in sight_result:
                        sr['link']="http://" + self.request.host + "/html/tour.html?Sight&key=" + sr['name_cn'] + ""

                list = {'country':country_result,'city':city_result,'sight':sight_result}
                self.write(list)

 
        except Exception as e:
            print('EXCEPTION FROM SearchHandler get:')
            print(e)
            self.write('{"status":101,"msg":"服务器繁忙中~!"}')
