#!/usr/local/bin/python3.5
#-*- coding: UTF-8 -*-

import sys
import tornado.web

import urllib.parse
import tornado.escape

sys.path.append('/var/server/beejeen/model')

from Tag   import Tag




class TagHandler(tornado.web.RequestHandler):
    def get(self, path):
        try:
            tag_id = self.get_argument('tag_id',0)
            limit  = self.get_argument('limit', 5)


            if tag_id == '0' :
                where = '1'
            else:
                where ='id= '+ tag_id + ''

            Tag_result = Tag.fetchOne( where , 'id,name_cn,name_en,create_time,sort', limit,' ORDER BY sort DESC')

             
            list = {'taglist':Tag_result}
            self.write(list)

           
        except Exception as e:
            print('EXCEPTION FROM Tag get:')
            print(e)
            self.write('{"status":101,"msg":"服务器繁忙中~!"}')
