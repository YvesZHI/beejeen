#!/usr/local/bin/python3.5
#-*- coding: UTF-8 -*-

import sys
import tornado.web

import time
import urllib.parse
import tornado.escape

sys.path.append('/var/server/beejeen/model')

from mWechatUser import mWechatUser


class mWechatAuthHandler(tornado.web.RequestHandler):
    def get(self, path):
        try:
            print('mWechatAuthHandler')
            print(self.request.uri)
            target = str(self.get_argument('url'))
            #url = self.get_argument('urlrefer')
            userinfo = mWechatUser.getUserInfo(self.get_argument('code'))
            user = mWechatUser(userinfo['openid'], userinfo['nickname'], userinfo['headimgurl'])
            user.add()
            target = urllib.parse.unquote(target) + '&user=' + userinfo['nickname'] + '&profile=' + userinfo['headimgurl'] + '&userid=' + str(mWechatUser.getId(userinfo['openid']))
            self.redirect(target)
            print('===========================================================')
        except Exception as e:
            print('EXCEPTION FROM mWechatAuthHandler get:')
            print(e)
            self.write('没有授权：用第三方登录的微信帐号无法授权微信第三方登录')
