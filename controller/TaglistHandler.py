#!/usr/local/bin/python3.5
#-*- coding: UTF-8 -*-

import sys
import tornado.web

import urllib.parse
import tornado.escape

sys.path.append('/var/server/beejeen/model')

from Taglist    import Taglist
from Sight      import Sight
from Hotel      import Hotel
from Restaurant import Restaurant
from Activity   import Activity



class TaglistHandler(tornado.web.RequestHandler):
    def get(self, path):
        try:

            tag_id = self.get_argument('tag_id', 1)
            limit  = self.get_argument('limit', 6)
            Taglist_result =''

            Taglist_result = Taglist.fetchOne('tag_id= '+ tag_id + '', 'id,type,poi_id,create_time', limit)
            print(Taglist_result)

            sight_arr    = []
            Hotel_arr    = {}
            Restr_arr    = {}
            Activity_arr = {}
            data_arr     = []
            

            #循环判断表
            if Taglist_result is not None:
                for tag in Taglist_result:
                    if tag['type']==1:

                        sight_arr   = Sight.getSightAndCountry('s.id= '+ str(tag['poi_id']) + '','s.id,s.name_cn,s.knownFor_cn as intro_cn,s.thumb_link,c.name_cn as country')
                        if sight_arr is not None:
                            sight_arr['type']="景点"
                            sight_arr['intro_cn'] = sight_arr['intro_cn'][0:23]
                            sight_arr['link']="http://" + self.request.host + "/html/tour.html?Sight&key="+sight_arr['name_cn']
                    
                            data_arr.append(sight_arr)

                   
                    elif  tag['type'] == 2:
                        Hotel_arr    = Hotel.getHotelAndCountry('h.id= '+ str(tag['poi_id']) + '','h.id,h.name_cn,h.intro_cn,h.thumb_link,c.name_cn as country')
                        if Hotel_arr is not None:                            
                            Hotel_arr['type']='酒店'
                            Hotel_arr['intro_cn'] = Hotel_arr['intro_cn'][0:23]
                            Hotel_arr['link']="http://" + self.request.host + "/html/tour.html?Sight&key="+Hotel_arr['name_cn']
                    
                            data_arr.append(Hotel_arr)
                    elif  tag['type'] == 3:
                        Restaurant_arr    = Restaurant.getRestaurantAndCountry('r.id= '+ str(tag['poi_id']) + '','r.id,r.name_cn,r.intro_cn,r.thumb_link,c.name_cn as country')
                        if Restaurant_arr is not None:
                            Restaurant_arr['type']="餐厅"
                            Restaurant_arr['intro_cn'] = Restaurant_arr['intro_cn'][0:23]
                            Restaurant_arr['link']="http://" + self.request.host + "/html/tour.html?Sight&key="+Restaurant_arr['name_cn']
                            data_arr.append(Restaurant_arr)
                    else:
                   
                        Activity_arr = Activity.getActivityAndCountry('a.id= '+ str(tag['poi_id']) + '','a.id,r.name_cn,a.intro_cn,a.thumb_link,c.name_cn as country')
                        if  Activity_arr is not None:
                            Activity_arr['type']="活动"
                            Activity_arr['intro_cn'] = Activity_arr['intro_cn'][0:23]
                            Activity_arr['link']="http://" + self.request.host + "/html/tour.html?Sight&key="+Activity_arr['name_cn']
                            data_arr.append(Activity_arr)
            else:
                data_arr ={}

            list = {'taglist':data_arr}
            self.write(list)

           
        except Exception as e:
            print('EXCEPTION FROM Taglist get:')
            print(e)
            self.write('{"status":101,"msg":"服务器繁忙中~!"}')
