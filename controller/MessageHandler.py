#!/usr/local/bin/python3.5
#-*- coding: UTF-8 -*-

import sys
import tornado.web
import re
import urllib.parse
import tornado.escape
import time

sys.path.append('/var/server/beejeen/model')

from Message import Message



class MessageHandler(tornado.web.RequestHandler):
    def post(self, path):
        try:
            print('MessageHandler')

            name            = self.get_argument('name', '')
            destination     = self.get_argument('destination', '')
            day             = self.get_argument('day', '')
            phone           = self.get_argument('phone', '')
            message         = self.get_argument('message', '')
            print(name)
            print(destination)
            print(day)
            print(phone)
            print(message)

            if not name:
                self.write('{"status":100,"msg":"请输入正确姓名!"}')
                return
            if not destination:
                self.write('{"status":101,"msg":"请输入正确目的地!"}')
                return
            if not phone:
                self.write('{"status":102,"msg":"请输入正确联系方式!"}')
                return 
            if not message :
                self.write('{"status":102,"msg":"请输入正确留言!"}')
                return

            RULE = re.compile("^[1][3,4,5,7,8,9,6][0-9]{9}$")
            phonematch = RULE.match(phone)
 
            if not phonematch:
                self.write('{"status":102,"msg":"请输入正确手机格式!!"}')
                return
                

            create_time = int(time.time())
            message_result = Message.addOne(name, destination,day,phone,message, create_time)
            if message is not None:
                list ={'status':200,'message':message_result}
            else:
                list = {'status': 100, 'message': message_result}
            self.write(list)

        except Exception as e:
            print('EXCEPTION FROM MessageHandler get:')
            print(e)
            self.write('{"status":101,"msg":"服务器繁忙中~!"}')
