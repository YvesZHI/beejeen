#!/usr/local/bin/python3.5
#-*- coding: UTF-8 -*-


import time
import random
import string
import hashlib
import json
import urllib.request

from weixin.client import WeixinMpAPI

from ConnectionDB import ConnectionDB
from Data import Data


class mWechatUser(Data):
    APP_ID = 'wx609470d336ba0b9b'
    APP_SECRET = '96700c5a5b64066c60e8eda4ee532aa8'
    scope = ("snsapi_userinfo", )
    api = None

    def getUrl(url):
        mWechatUser.api = WeixinMpAPI(appid=mWechatUser.APP_ID, app_secret=mWechatUser.APP_SECRET, redirect_uri=url)
        return mWechatUser.api.get_authorize_url(scope=mWechatUser.scope)

    def getUserInfo(code):
        try:
            auth_info = mWechatUser.api.exchange_code_for_access_token(code=code)
            data = WeixinMpAPI(access_token=auth_info['access_token'])
            return data.user(openid=auth_info['openid'])
        except Exception as e:
            print(e)
            raise

    def __init__(self, popenid, pname, pprofile):
        super(mWechatUser, self).__init__(pname)
        self.openid = popenid
        self.profile = pprofile

    def add(self):
        query = 'select count(*) from User where third_party_id = "' + str(self.openid) + '";'
        res = self.condb.executeCurFetchOne(query)
        if res[0] > 0:
            return False
        query = 'insert into User (name, profile, third_party_id, type) values (\'' + self.name + '\', \'' + self.profile + '\', \'' + str(self.openid) + '\', 1);'
        self.condb.commit(query)
        return True

    def getId(popenid):
        query = 'select id from User where third_party_id = \'' + popenid + '\';'
        condb = ConnectionDB()
        res = condb.executeCurDictFetchOne(query)
        return res['id']

    def getJson(self, lang):
        pass
