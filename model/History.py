#!/usr/local/bin/python3.5
#-*- coding: UTF-8 -*-
from ConnectionDB import ConnectionDB
from Data import Data


class History(Data):
    def __init__(self, pname):
        super(History, self).__init__(pname)

    def fetchAll(where,field="*", num=9):
        if not where:
            return

        query = 'select ' + field + ' from History where ' + where + ' group by keyword' + ' limit '+ str(num)
         
        condb = ConnectionDB()
        res = condb.executeCurDictFetchAll(query)
         
        return res

    def fetchOne(where,field="*"):    
        if not where:
            return
        query = 'select ' + field + ' from History where ' + where
         
        condb = ConnectionDB()
        res = condb.executeCurDictFetchOne(query)
        return res

    def addOne(sess_id,keyword,create_time):
        if not keyword:
            return
        if not sess_id:
            return
        if not create_time:
            return
        query = 'insert into History (keyword, sess_id, create_time) values (\'' + str(keyword) + '\',\'' + str(sess_id) + '\', \'' + str(create_time) +  '\');'
         
        condb = ConnectionDB()
        res = condb.commit(query)
        return res